package edu.uprm.cse.datastructures.cardealer;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Predicate;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;
import edu.uprm.cse.datastructures.cardealer.util.SortedCircularDoublyLinkedList;

@Path("/cars")
public class CarManager {



	private final HashTableOA<Long, Car> cList = CarTable.getInstance();

	@GET
	@Produces(MediaType.APPLICATION_JSON) 
	public Car[] getAllCars() { 		//gets todos los carros del array MUST BE SORTED
		Car[] result = new Car[cList.size()];
//		for (int i = 0; i < cList.size(); i++) {
//			result[i] = cList.get(i);
//		}
		return result;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){	//if id's match devuelve el carro del array
//		for (int i = 0; i < cList.size(); i++) {
//			if (cList.get(i).getCarId() == id) {
//				return cList.get(i);
//			}
//		}
		throw new WebApplicationException(404);
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)	
	public Response addCar(Car car){		//añade carro a cList
//		cList.add(car);
		return Response.status(Response.Status.CREATED).build();
	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){			//remueve un carro en específico y añade un carro nuevo 
		for (int i = 0; i < cList.size(); i++) {
//			if (cList.get(i).getCarId() == car.getCarId()) {
//				cList.remove(i);
//				cList.add(car);
//				return Response.status(200).build();
//			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){	//borra carro del array
//		for (int i = 0; i < cList.size(); i++) {
//			if (cList.get(i).getCarId() == id) {
//				cList.remove(i);
//				return Response.status(200).build();
//			}
//		}
		return Response.status(Response.Status.NOT_FOUND).build();

	}

}
