package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.List;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class HashTableOA<K,V> implements Map<K,V>{


	public static class MapEntry<K,V> {
		private K key;
		private V value;

		public K getKey() {
			return key;
		}
		public void setKey(K key) {
			this.key = key;
		}
		public V getValue() {
			return value;
		}
		public void setValue(V value) {
			this.value = value;
		}
		public MapEntry(K key, V value) {
			super();
			this.key = key;
			this.value = value;
		}

	}

	private int currentSize;
	private Object[] buckets;
	private static final int DEFAULT_BUCKETS = 10;


	private int firstHashFunction(K key) {
		return key.hashCode() * 983 % this.buckets.length; //key*primo
	}

	private int secondHashFunction(K key) {
		//return (key.hashCode()) * (key.hashCode()) % this.buckets.length; //el que dio manuel
		return this.firstHashFunction(key) * 997 % this.buckets.length; //primer hashFunction * primo
	}


	public HashTableOA() {
		currentSize = 0;
		buckets = new Object[DEFAULT_BUCKETS];
		for (int i = 0; i < buckets.length; i++) {
			buckets[i] = new MapEntry<K,V>(null,null);
		}
	}


	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(K key) {
		int hashIndex = this.firstHashFunction(key);
		if (((MapEntry<K, V>)buckets[hashIndex]).getKey().equals(key)) {
			return ((MapEntry<K, V>)buckets[hashIndex]).getValue();
		}

		int hashIndex2 = this.secondHashFunction(key);
		if (((MapEntry<K, V>)buckets[hashIndex2]).getKey().equals(key)) {
			return ((MapEntry<K, V>)buckets[hashIndex2]).getValue();
		}

		for (int i = 0; i < buckets.length; i++) {
			if(((MapEntry<K, V>)buckets[hashIndex]).getKey().equals(key)) {
				((MapEntry<K, V>)buckets[hashIndex2]).getValue();
			}
			else {
				hashIndex2 = (hashIndex2 + 1) % this.buckets.length;
			}
		}
		return null;

	}

	@SuppressWarnings("unchecked")
	@Override
	public V put(K key, V value) {
		int hashIndex = this.firstHashFunction(key);
		if (((MapEntry<K, V>)buckets[hashIndex]).getKey() == null) {
			((MapEntry<K, V>)buckets[hashIndex]).setValue(value);
			((MapEntry<K, V>)buckets[hashIndex]).setKey(key);
			return null;
		}
		int hashIndex2 = this.secondHashFunction(key);
		if (((MapEntry<K, V>)buckets[hashIndex2]).getKey() == null) { 
			((MapEntry<K, V>)buckets[hashIndex2]).setValue(value);
			((MapEntry<K, V>)buckets[hashIndex2]).setKey(key);
			return null;
		}
		for (int i = 0; i < buckets.length; i++) {
			if(((MapEntry<K, V>)buckets[hashIndex2]).getKey() == null) {
				((MapEntry<K, V>)buckets[hashIndex2]).setValue(value);
				((MapEntry<K, V>)buckets[hashIndex2]).setKey(key);
			}
			else {
				hashIndex2 =(hashIndex2 + 1) % this.buckets.length;
			}

		}
		return value;

	}

	@SuppressWarnings("unchecked")
	@Override
	public V remove(K key) { // return old value/ set value y key a null
		int hashIndex = this.firstHashFunction(key);
		V oldValue = ((MapEntry<K, V>) buckets[hashIndex]).getValue();
		if (((MapEntry<K, V>)buckets[hashIndex]).getKey().equals(key)) {
			((MapEntry<K, V>)buckets[hashIndex]).setValue(null);
			((MapEntry<K, V>)buckets[hashIndex]).setKey(null);
			return oldValue;
		}

		int hashIndex2 = this.secondHashFunction(key);
		V oldValue2 = ((MapEntry<K, V>)buckets[hashIndex2]).getValue();
		if (((MapEntry<K, V>)buckets[hashIndex2]).getKey().equals(key)) {
			((MapEntry<K, V>)buckets[hashIndex2]).setValue(null);
			((MapEntry<K, V>)buckets[hashIndex2]).setKey(null);
			return oldValue2;

		}

		for (int i = 0; i < buckets.length; i++) {
			if(((MapEntry<K, V>)buckets[hashIndex]).getKey().equals(key)) {
				((MapEntry<K, V>)buckets[hashIndex]).setValue(null);
				((MapEntry<K, V>)buckets[hashIndex]).setKey(null);
			}
			else {
				hashIndex2 = (hashIndex2 + 1) % this.buckets.length;
			}
		}
		return null;
	}

	@Override
	public boolean contains(K key) {
		//		if (this.get(key) == null) {
		//			return false;
		//		}
		//		return true;

		return this.get(key) != null;
	}

	@Override
	public  List<K> getKeys() { //sortedlist con todos los keys
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<V> getValues() { //sortedlist con todos los values
		// TODO Auto-generated method stub
		return null;
	}



}
