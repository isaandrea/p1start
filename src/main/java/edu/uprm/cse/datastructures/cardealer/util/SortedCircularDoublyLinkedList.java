package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class SortedCircularDoublyLinkedList<E>  implements SortedList<E> {

	private Node<E> header;
	private int currentSize;
	private Comparator <E> Comp;


	public SortedCircularDoublyLinkedList(Comparator<E> comp) {
		header = new Node<E>();
		header.setPrev(header);
		header.setNext(header);
		this.currentSize = 0;
		this.Comp = comp;

	}

	private static class Node<E>{
		private E element;
		private Node<E> next;
		private Node<E> prev;

		public Node(E element, Node<E> next, Node<E> prev) {
			super();
			this.element = element;
			this.next = next;
			this.prev = prev;
		}
		public Node(E element) {
			super();
			this.element = element;
		}
		public Node() {
			super();
			this.element = null;
			this.next = this.prev = null;

		}
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

	}

	@Override
	public Iterator iterator() {
		return new SortedCircularDoublyLinkedListIterator<E>();
	}
	private class SortedCircularDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;


		public SortedCircularDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}
		@Override
		public boolean hasNext() {
			return nextNode != header;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}

	}

	//creates new node with desired element and adds it to the list
	@Override
	public boolean add(E obj) {
		if (obj == null) {
			throw new IllegalArgumentException("This object can't be null.");
		}

		if (this.isEmpty()) {
			Node<E> n = new Node<E>(obj, header, header);
			this.header.setNext(n);
			this.header.setPrev(n);
			currentSize++;
			return true;
		}

		Node <E> n = new Node<E>(obj);
		for (Node<E> temp = this.header.getNext(); temp !=this.header; temp = temp.getNext()) {
			if (this.Comp.compare(obj, temp.getElement()) <= 0){
				n.setNext(temp);
				n.setPrev(temp.getPrev());
				temp.getPrev().setNext(n);
				temp.setPrev(n);
				this.currentSize++;
				return true;
			}
		}

		n.setNext(header);
		n.setPrev(header.getPrev());
		header.getPrev().setNext(n);
		header.setPrev(n);
		this.currentSize++;

		return true;
	}

	//return size of list
	@Override
	public int size() {
		return this.currentSize;
	}

	//removes copy of an obj
	@Override
	public boolean remove(E obj) {
		if (this.contains(obj)) {
			return this.remove(this.firstIndex(obj));
		}
		return false;

	}

	//removes element at index i
	@Override
	public boolean remove(int index) {
		if (index<0 || index >= this.size()) {
			throw new IndexOutOfBoundsException ("Index is not in acceptable range.");
		}
		int counter = 0;
		Node <E> temp = this.header.getNext();
		while(counter != index) {
			counter++;
			temp = temp.getNext();
		}
		temp.getPrev().setNext(temp.getNext());
		temp.getNext().setPrev(temp.getPrev());
		temp.setElement(null);
		temp.setNext(null);
		temp.setPrev(null);
		this.currentSize--;
		return true;

	}

	//removes all copies of obj
	@Override
	public int removeAll(E obj) {
		int count = 0;
		while(remove(obj)) {
			count++;
		}
		return count;

	}

	//returns first element of list
	@Override
	public E first() {
		return this.isEmpty() ? null: this.header.getNext().getElement();
	}

	//returns last element of list
	@Override
	public E last() {
		return this.isEmpty() ? null: this.header.getPrev().getElement();
	}

	
	@Override
	public E get(int index) {
		if (index<0 || index >= this.size()) {
			throw new IndexOutOfBoundsException ("Index is not in acceptable range.");
		}
		int counter = 0;
		Node <E> temp = new Node<E>();
		for (temp = this.header.getNext(); counter < index; temp = temp.getNext(), counter++);
		return temp.getElement();
	}

	//removes all elements of list
	@Override
	public void clear() {
		while (!this.isEmpty()) {
			this.remove(0);
		}

	}

	//checks if list contains element e
	@Override
	public boolean contains(E e) {
		if (this.isEmpty()) {
			return false;
		}
		else {
			for (Node<E> temp = header.getNext(); temp != header; temp = temp.getNext()) {
				if (temp.getElement().equals(e)) {
					return true;
				}
			}
		}
		return false;
	}

	//checks if list is empty
	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	//finds index of first element e
	@Override
	public int firstIndex(E e) {
		int i = 0;
		Node<E> temp = this.header.getNext();
		while(temp != this.header) {
			if(Comp.compare(temp.getElement(), e) == 0)
				return i;
			i++;
			temp = temp.getNext();
		}
		// not found
		return -1;
	}

	//finds index of last element e
	@Override
	public int lastIndex(E e) {
		if (this.isEmpty()) {
			return -1;
		}
		int index = this.currentSize-1;
		Node<E> temp = header.getPrev();
		while(temp != header) {
			if (Comp.compare(e, temp.getElement()) == 0) {
				return index;
			}
			index--;
			temp = temp.getPrev();
		}
		return -1;
	}

}
