package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

public class CarTable {


	  private static HashTableOA<Long, Car> cList = new HashTableOA<Long, Car>();
	  
	  //private CarList(){}
	  
	  public static HashTableOA<Long, Car> getInstance() { // devuelve nueva lista de cars
	    return cList;
	  }
	  
	  public static void resetCars() { // vacia la lista
		  cList = new HashTableOA<>();
	  }
	  
	}


